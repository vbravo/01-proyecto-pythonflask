import requests
import json
from flask import Flask, redirect, render_template, url_for, jsonify, request
from flaskext.mysql import MySQL
import pymysql
import pymysql.cursors

 
"""  
 Victor Sebastian Bravo Barrera

 """


app = Flask(__name__)
app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_DB"]  = "vacunatorio"

mysql = MySQL(app)
mysql.connect_args["autocommit"] = True
mysql.connect_args["cursorclass"] = pymysql.cursors.DictCursor


@app.route('/',  methods=["GET","POST"])
def pacientes():
	cursor = mysql.get_db().cursor()
	sql = "SELECT * FROM paciente"
	cursor.execute(sql)
	paciente_ = cursor.fetchall()
	return render_template("pacientes.html", paciente=paciente_) 

@app.route('/add',  methods=["GET","POST"])
def AgregarPaciente():
	return render_template("agregarPaciente.html") 


@app.route('/guardar',  methods=["POST"])
def GuardarPaciente():

	cursor = mysql.get_db().cursor()
	
	print(request.form)
	nombre = request.form["nombre"]
	rut = request.form["rut"]
	fechan = request.form["fechan"]


	sql = "INSERT INTO paciente (nombre,rut,fecha_nacimiento) VALUES (%s,%s,%s)"
	cursor.execute(sql,(nombre,rut,fechan))

	return redirect(url_for('pacientes'))
	""" return render_template("pacientes.html", paciente=paciente_) """
 
@app.route('/vacunas',  methods=["GET","POST"])
def vacunas():
	cursor = mysql.get_db().cursor()
	sql = "SELECT * FROM vacuna"
	cursor.execute(sql)
	vacuna_ = cursor.fetchall()
	return render_template("vacunas.html", vacuna=vacuna_) 

@app.route('/addVacuna',  methods=["GET","POST"])

def AgregarVacuna():
	return render_template("nuevaVacuna.html") 

@app.route('/guardarVacuna',  methods=["GET","POST"])
def GuardarVacuna():
	cursor = mysql.get_db().cursor()
	nombre_enfermedad = request.args["nombre_enfermedad"]
	sql = "INSERT INTO vacuna (nombre_enfermedad) VALUES (%s)"
	cursor.execute(sql,(nombre_enfermedad))
	
	return redirect(url_for('vacunas'))



@app.route('/pacVac',  methods=["GET","POST"])
def pacientesVacuna():
	pacienteID = request.form["id_paciente"]
	
	cursor = mysql.get_db().cursor()
	sql = "SELECT * FROM paciente WHERE paciente.id_paciente ="+pacienteID

	cursor.execute(sql)
	paciente_ = cursor.fetchall()

	sql2 = "SELECT * FROM vacuna where vacuna.id_vacuna NOT IN(SELECT id_vacuna FROM recibe WHERE id_paciente ="+pacienteID+")"
	cursor.execute(sql2)
	vacuna_ = cursor.fetchall()
	
	return render_template("pacienteVa.html", paciente=paciente_, vacuna=vacuna_) 	
	

@app.route('/vacunarPaciente',  methods=["GET","POST"])
def vacunarPacientes():
	
	pacienteID = request.form["id_paciente"]
	vacunaID = request.form.get("vacuna")
	if vacunaID!=None:
		cursor = mysql.get_db().cursor()
		sql = "INSERT INTO recibe (id_paciente,id_vacuna) VALUES (%s,%s)"
		cursor.execute(sql,(pacienteID,vacunaID))
	else:
		return("El paciente ya esta con todas sus vacunas")
	return redirect(url_for('pacientes'))


@app.route('/verVacunados',  methods=["GET","POST"])
def verVacunados():
	pacienteID = request.form["id_paciente"]
	
	cursor = mysql.get_db().cursor()
	sql = "SELECT vacuna.nombre_enfermedad, recibe.created_at FROM recibe INNER JOIN paciente ON paciente.id_paciente = recibe.id_paciente INNER JOIN vacuna ON vacuna.id_vacuna = recibe.id_vacuna AND paciente.id_paciente ="+pacienteID

	cursor.execute(sql)
	paciente_ = cursor.fetchall()


	return render_template("verVacunados.html", paciente=paciente_) 	


@app.route('/verVacunasPersonas',  methods=["GET","POST"])
def verVacunasPersonas():
	vacunaID = request.form["id_vacuna"]
	
	cursor = mysql.get_db().cursor()
	sql = "SELECT paciente.nombre,paciente.rut,recibe.created_at FROM recibe INNER JOIN paciente ON paciente.id_paciente = recibe.id_paciente INNER JOIN vacuna ON vacuna.id_vacuna = recibe.id_vacuna AND vacuna.id_vacuna ="+vacunaID

	cursor.execute(sql)
	paciente_ = cursor.fetchall()


	return render_template("verVacunaPaciente.html", paciente=paciente_) 					



if __name__ == "__main__":
	app.run(debug=True)





	